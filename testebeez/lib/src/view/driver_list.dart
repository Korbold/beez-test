import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/material.dart';
import 'package:testebeez/src/controller/driver_controller.dart';

class DriverList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final driver_con = new DriverController();

    return Scaffold(
      appBar: AppBar(
        title: Text('Drivers'),
      ),
      body: StreamBuilder(
        stream: driver_con.response(),
        builder: (_, AsyncSnapshot<dynamic> snapshot) {
          final data = snapshot.data ?? [];
          return ListView.builder(
              itemCount: data.length,
              itemBuilder: (_, i) {
                return Card(
                  elevation: 4,
                  child: ListTile(
                    leading: CircularProfileAvatar(
                      'https://avatars0.githubusercontent.com/u/8264639?s=460&v=4',
                      radius: 30,
                      borderWidth: 3,
                      borderColor: Colors.blueGrey,
                    ),
                    title: Text(data[i]["name"]),
                    trailing: Icon(Icons.transit_enterexit_rounded),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        LightStatus(
                          data: data,
                          i: i,
                          str: "status",
                        ),
                        LightStatus(
                          data: data,
                          i: i,
                          str: "is_active",
                        ),
                        LightStatus(
                          data: data,
                          i: i,
                          str: "is_working",
                        )
                      ],
                    ),
                  ),
                );
              });
        },
      ),
    );
  }
}

class LightStatus extends StatelessWidget {
  const LightStatus({
    Key? key,
    required this.data,
    required this.i,
    required this.str,
  }) : super(key: key);

  final data;
  final int i;
  final String str;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 20,
      height: 20,
      decoration: BoxDecoration(
          color: data[i][str] == 1 ? Colors.green : Colors.red,
          shape: BoxShape.circle),
    );
  }
}
