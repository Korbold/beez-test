import 'dart:convert';
import 'package:flutter/services.dart';

class DriverRepository {
  Future<List<dynamic>> dataCarrier() async {
    final jsonFile = await rootBundle.loadString('assets/active_carrier.json');
    List<dynamic> jsonString = json.decode(jsonFile);

    return jsonString;
  }
}
