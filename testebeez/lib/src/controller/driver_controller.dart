import 'package:testebeez/src/model/driver_repository/driver_repository.dart';

class DriverController {
  final _driverRepository = new DriverRepository();

  Stream response() async* {
    yield await _driverRepository.dataCarrier();
  }
}
