// @dart=2.9
import 'package:testebeez/src/view/driver_list.dart';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: DriverList(),
    );
  }
}
